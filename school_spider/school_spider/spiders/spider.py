#coding=utf-8
import scrapy
from urllib.request import urlopen, urljoin
import re
import os
import urllib
import MySQLdb
import sys
import datetime
import hashlib
from scrapy.selector import Selector
from scrapy.http import HtmlResponse,Request
from scrapy_redis.spiders import RedisSpider
from school_spider.items import SchoolSpiderItem #导入item对应的类，crawlPictures是项目名，items是items.py文件，import的是items.py中的class，也可以import *

class SchoolSpider(RedisSpider):
    name="school_spider"    #定义爬虫名，要和settings中的BOT_NAME属性对应的值一致
    nowTime=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    allowed_domains=["scnu.edu.cn"] #搜索的域名范围，也就是爬虫的约束区域，规定爬虫只爬取这个域名下的网页
    #start_urls=["http://www.scnu.edu.cn",]   #开始爬取的地址
    redis_key="school_spider:start_urls"    

    #该函数名不能改变，因为Scrapy源码中默认callback函数的函数名就是parse
    def parse(self, response):
        md5_value = hashlib.md5()
        se=Selector(response) #创建查询对象，HtmlXPathSelector已过时
        item=SchoolSpiderItem()  #实例item（具体定义的item类）,将要保存的值放到事先声明的item属性中
        charset_list=response.xpath("//meta[@http-equiv='Content-Type']").re(r'charset=([-0-9A-Za-z]*)')
        if not charset_list: #没有charset的情况
            charset="utf-8"
        else:
            charset=charset_list[0]
        item['domain']=re.search(r'[-a-z0-9A-Z]+\.scnu\.edu\.cn',response.url).group()
        #print (item['domain'])
        try:
            item['content']=response.body.decode(charset)
        except:
            item['content']=response.body.decode("utf-8")
        md5_value.update(response.body)
        item['md5']=md5_value.hexdigest()
        item['headers']=response.headers
        item['url']=response.url
        item['time_stamp']=self.nowTime
        #print (item["content"],item["headers"],item["url"])    
        yield item  #返回item,这时会自定解析item
        all_urls=se.xpath("//a/@href").extract()#提取界面所有的url
        #print (all_urls)
        for url in all_urls:
            if(re.search(r'(\.iso|\.pdf|\.jpg|\.v|\.doc|\.docx|\.jpg|\.jpeg|\.ppt|\.pptx|\.mp4|\.mp3|\.rar|\.zip|\.xls|\.xlsx|\.png|\.exe|\.swf){1}$',url) == None or re.search(r'bbs.scnu.edu.cn')==None): #过滤文件
                yield Request(urljoin(response.url,url),callback=self.parse)


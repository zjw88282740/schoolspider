# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class SchoolSpiderItem(scrapy.Item):
    '''定义需要格式化的内容（或是需要保存到数据库的字段）'''
    # define the fields for your item here like:
    # name = scrapy.Field()
    domain = scrapy.Field()
    url = scrapy.Field()   #修改你所需要的字段
    content = scrapy.Field()
    headers = scrapy.Field()
    md5 = scrapy.Field()
    time_stamp = scrapy.Field()
